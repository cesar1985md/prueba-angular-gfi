# PruebaAngular
Angular 2+ test by GFI.
This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.0.8.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Prerequisites

The next libraries was used on the proyect.
- Bootstrap 4 (https://getbootstrap.com/docs/4.0/getting-started/introduction/)
- PrimeNG (https://primefaces.org/primeng/#/)
- Reddux (https://es.redux.js.org/)
- Api (http://www.omdbapi.com/, KEY: f12ba140)
- Git Repository: (https://gitlab.com/cesar1985md/prueba-angular-gfi.git)

## Acces to Admin
You can log in with this users:
    Usuario(1, 'cesar@gmail.com', 'cesar1103'),
    Usuario(2, 'mario@gmail.com', 'mario1103'),
    Usuario(3, 'jorge@gmail.com', 'jorge1103'),

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

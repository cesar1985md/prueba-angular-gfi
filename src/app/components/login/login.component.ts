import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Usuario } from '../../classes/peliculas';

import { ReactiveFormsModule, FormGroup, FormControl, FormsModule, Validators } from '@angular/forms';
import { PeliculasService } from '../../services/peliculas.service';

import { ActivatedRoute, Router } from '@angular/router';
import { ListadoComponent } from '../listado/listado.component';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
    public usuario: Usuario;
    public formLogin: FormGroup;
    public acceso: boolean;
    public errorAcceso: boolean;

    @Output() public accessControl: EventEmitter<boolean>;

    constructor(
        private peliculasService: PeliculasService,
        private router: Router
    ) {
        this.usuario = new Usuario(null, '', '');
        this.accessControl = new EventEmitter<boolean>();
        this.acceso = false;
        this.errorAcceso = false;

        // Creo el formualio y le asigno los validadores a cada campo, he puesto required pero se pueden agregar los que se deseen
        this.formLogin = new FormGroup({
            'email': new FormControl(this.usuario.email, [Validators.required]),
            'pass': new FormControl(this.usuario.pass, [Validators.required])
        });
    }

    ngOnInit() {

    }

    public onSubmit() {
        this.usuario = this.formLogin.value;
        if (this.peliculasService.controlAcceso(this.usuario)) {
            this.router.navigate(['listado']);
        } else {
            this.errorAcceso = true;
        }
    }
}

import { Component, OnInit } from '@angular/core';
import { Pelicula } from '../../classes/peliculas';
import { ActivatedRoute } from '@angular/router';
import { PeliculasService } from '../../services/peliculas.service';

import { RatingModule } from 'primeng/rating';


@Component({
    selector: 'app-detalle',
    templateUrl: './detalle.component.html',
    styleUrls: ['./detalle.component.css']
})
export class DetalleComponent implements OnInit {
    public idPelicula: string;
    public peliculaDetalle: Pelicula;
    public val: number;
    constructor(
        private route: ActivatedRoute,
        private peloculasService: PeliculasService
    ) {
        this.idPelicula = null;
        this.peliculaDetalle = null;
        this.val = 3;
    }

    ngOnInit() {
        this.route.params.subscribe(params => {
            this.idPelicula = params['id'];
            this.peloculasService.searchFilm(this.idPelicula).subscribe( p => {
                this.peliculaDetalle = p.body;
            });
        });
    }

}

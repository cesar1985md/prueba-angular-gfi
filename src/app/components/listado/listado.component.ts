import { Component, OnInit } from '@angular/core';
import { SessionStorageService, SessionStorage } from 'angular-web-storage';
import { Sesion, Pelicula } from '../../classes/peliculas';
import { PeliculasService } from '../../services/peliculas.service';
import { Router } from '@angular/router';

@Component({
    selector: 'app-listado',
    templateUrl: './listado.component.html',
    styleUrls: ['./listado.component.css']
})
export class ListadoComponent implements OnInit {
    public currentSesion: Sesion;
    public filtro: string;
    public peliculas: Array<Pelicula>;
    public favoritos: Array<Pelicula>;
    public display: boolean;
    constructor(
        private session: SessionStorageService,
        private peliculasService: PeliculasService,
        private router: Router
    ) {
        this.currentSesion = new Sesion();
        this.filtro = '';
        this.peliculas = [];
        this.favoritos = [];
        this.display = false;
    }

    ngOnInit() {
        this.currentSesion = this.session.get('sesion');
        if (localStorage.getItem('favoritos')) {
            this.favoritos = JSON.parse(localStorage.getItem('favoritos'));
        }
    }

    cerrarSesion() {
        this.peliculasService.deleteSesion();
        this.router.navigate(['/']);
    }

    buscarPeliculas() {
        this.peliculas = [];
        this.peliculasService.searchFilmListado(this.filtro).subscribe(res => {
            this.peliculas = res.body.Search;
        },
            err => {
                console.log('Error en la solicitud de listado');
            });
    }

    addFavorito(pelicula: Pelicula) {
        this.peliculasService.addFavoriteFilm(pelicula);
        this.favoritos = JSON.parse(localStorage.getItem('favoritos'));
    }
}

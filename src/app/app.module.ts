import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { DialogModule } from 'primeng/dialog';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppComponent } from './app.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { PeliculasService } from './services/peliculas.service';
import { AngularWebStorageModule } from 'angular-web-storage';
import { RouterModule, Routes } from '@angular/router';
import { ListadoComponent } from './components/listado/listado.component';
import { AuthGuardService as AuthGuard } from './guards/auth-guard.service';
import { DetalleComponent } from './components/detalle/detalle.component';
import { LoginComponent } from './components/login/login.component';
// Modulos de Prime
import { RatingModule } from 'primeng/rating';
import { MessagesModule } from 'primeng/messages';
import { MessageModule } from 'primeng/message';

const appRoutes: Routes = [
    { path: 'login', component: LoginComponent },
    { path: 'listado', component: ListadoComponent, canActivate: [AuthGuard]},
    { path: 'detalle/:id', component: DetalleComponent, canActivate: [AuthGuard]},
    { path: '**', component: LoginComponent }
];

@NgModule({
    declarations: [
        AppComponent,
        LoginComponent,
        ListadoComponent,
        DetalleComponent
    ],
    imports: [
        BrowserModule,
        FormsModule,
        HttpClientModule,
        ReactiveFormsModule,
        AngularWebStorageModule,
        BrowserAnimationsModule,
        DialogModule,
        RatingModule,
        MessagesModule,
        MessageModule,
        RouterModule.forRoot(
            appRoutes,
            { enableTracing: false }
        )
    ],
    providers: [PeliculasService],
    bootstrap: [AppComponent]
})
export class AppModule { }

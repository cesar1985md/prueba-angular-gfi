export class Pelicula {
    public Actors: string;
    public Awards: string;
    public BoxOffice: string;
    public Country: string;
    public DVD: string;
    public Director: string;
    public Genre: string;
    public Language: string;
    public Metascore: string;
    public Plot: string;
    public Poster: string;
    public Production: string;
    public Rated: string;
    public Ratings: Ratings;
    public Released: string;
    public Response: string;
    public Runtime: string;
    public Title: string;
    public Type: string;
    public Website: string;
    public Writer: string;
    public Year: string;
    public imdbID: string;
    public imdbRating: string;
    public imdbVotes: string;
    constructor (peliculas: Pelicula) {
        this.Actors = peliculas.Actors ? peliculas.Actors : '';
        this.Awards = peliculas.Awards ? peliculas.Awards : '';
        this.BoxOffice = peliculas.BoxOffice ? peliculas.BoxOffice : '';
        this.DVD = peliculas.DVD ? peliculas.DVD : '';
        this.Director = peliculas.Director ? peliculas.Director : '';
        this.Genre = peliculas.Genre ? peliculas.Genre : '';
        this.Language = peliculas.Language ? peliculas.Language : '';
        this.Metascore = peliculas.Metascore ? peliculas.Metascore : '';
        this.Plot = peliculas.Plot ? peliculas.Plot : '';
        this.Poster = peliculas.Poster ? peliculas.Poster : '';
        this.Production = peliculas.Production ? peliculas.Production : '';
        this.Rated = peliculas.Rated ? peliculas.Rated : '';
        this.Ratings = peliculas.Ratings ? new Ratings (peliculas.Ratings.Source, peliculas.Ratings.Value) : new Ratings('', '');
        this.Released = peliculas.Released ? peliculas.Released : '';
        this.Response = peliculas.Response ? peliculas.Response : '';
        this.Runtime = peliculas.Runtime ? peliculas.Runtime : '';
        this.Title = peliculas.Title ? peliculas.Title : '';
        this.Type = peliculas.Type ? peliculas.Type : '';
        this.Website = peliculas.Website ? peliculas.Website : '';
        this.Year = peliculas.Year ? peliculas.Year : '';
        this.imdbID = peliculas.imdbID ? peliculas.imdbID : '';
        this.imdbRating = peliculas.imdbRating ? peliculas.imdbRating : '';
        this.imdbVotes = peliculas.imdbVotes ? peliculas.imdbVotes : '';
    }
}

export class Ratings {
    public Source: string;
    public Value: string;
    constructor(Source: string, Value: string) {
        this.Source = Source;
        this.Value = Value;
    }
}

export class Usuario {
    public id: number;
    public email: string;
    public pass: string;
    constructor(id: number, email: string, pass: string) {
        this.id = id;
        this.email = email;
        this.pass = pass;
    }
}

export class Sesion {
    public token: string;
    public usuario: Usuario;
}

import { Injectable } from '@angular/core';
import { Usuario, Sesion, Pelicula } from '../classes/peliculas';
import { SessionStorageService, SessionStorage } from 'angular-web-storage';

import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ReturnStatement } from '@angular/compiler';

const API_KEY = 'f12ba140';
const URL = 'http://www.omdbapi.com/';

@Injectable()
export class PeliculasService {

    // Array donde guardo los usuarios que tienen acceso
    public usuarios: Array<Usuario>;
    public currentSesion: Sesion;
    public peliculas: Array<Pelicula>;
    constructor(
        public session: SessionStorageService,
        public _httpClient: HttpClient
    ) {
        this.usuarios = [];
        this.currentSesion = new Sesion();

        this.usuarios.push(
            new Usuario(1, 'cesar@gmail.com', 'cesar1103'),
            new Usuario(2, 'mario@gmail.com', 'mario1103'),
            new Usuario(3, 'jorge@gmail.com', 'jorge1103'),
        );
        this.peliculas = [];
    }

    public controlAcceso(usuario: Usuario): boolean {
        let isLogged = false;
        if (usuario) {
            this.usuarios.forEach(e => {
                if (e.email === usuario.email && e.pass === usuario.pass) {
                    this.currentSesion.token = this.random();
                    this.currentSesion.usuario = usuario;
                    this.session.set('sesion', this.currentSesion); // Session Storage
                    isLogged = true;
                }
            });
        }
        return isLogged;
    }

    public isAuthenticated(): boolean {
        const userSession = this.session.get('sesion');
        if (userSession) {
            return true;
        } else {
            return false;
        }
    }

    public getSesion(): Sesion {
        if (this.session.get('sesion')) {
            return this.currentSesion;
        }
    }

    public deleteSesion() {
        this.currentSesion = new Sesion();
        localStorage.clear();
    }

    public searchFilm(filtro: string): Observable<HttpResponse<any>> {
        return this._httpClient.get(`${URL}?apikey=${API_KEY}&i=${filtro}`, { observe: 'response' });
    }

    public searchFilmListado(filtro: string): Observable<HttpResponse<any>> {
        return this._httpClient.get(`${URL}?apikey=${API_KEY}&s=${filtro}`, { observe: 'response' });
    }

    public addFavoriteFilm(pelicula: Pelicula) {
        this.peliculas.push(pelicula);
        localStorage.setItem('favoritos', JSON.stringify(this.peliculas));
    }

    // Funcion donde genero el token
    private random() {
        return Math.random().toString(36).substr(2);
    }
}

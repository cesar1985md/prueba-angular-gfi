import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { PeliculasService } from '../services/peliculas.service';


@Injectable({
    providedIn: 'root'
})
export class AuthGuardService {

    constructor(
        public usuario: PeliculasService, public router: Router
    ) { }

    canActivate(): boolean {
        if (!this.usuario.isAuthenticated()) {
            this.router.navigate(['login']);
            return false;
        }
        return true;
    }
}
